<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nevara');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'P.b38OWD,9g~p-Qwa/QhK5BGgq0XSu5aM^knde(||JKd$*=R2!I`<$&J&rP+WZbC');
define('SECURE_AUTH_KEY',  'd~NB~~69/Ixha{`,`p&pOd&,MBI.^eF|mNU9aq wN*^4JDT9[Nt8J>]SfiuncPY?');
define('LOGGED_IN_KEY',    '9M?D$,][h)r+[L:5jf{a}}5>!:OYZI]QI{4Ma16B7^d )Mm!Nz`VQwZ&e>f>)^?H');
define('NONCE_KEY',        '`wHt<D$sj`~G_<}Ben.bbx7gyt1=y]ut/F fK5w2vawR,GWf4RFPf`Pxj2nHAfUi');
define('AUTH_SALT',        'C-?bdYXXd+>g|JRyytgw_4<PIy7+b{K2Rll}%@k=i,-/n,CWc$k /wu[K/hCJvGC');
define('SECURE_AUTH_SALT', '.I3k;H.4ai^v+y8dqy/(bV^puY2#qI<++lp~1E]&XI&e~&~lwZ)yuH*C~7P)d~$E');
define('LOGGED_IN_SALT',   'Zd!~aWg! ~@,<dpFds$N679T;{90q:g@wF%uY<Z]KE%AdPj~~9Xq6H))$0K4[0<t');
define('NONCE_SALT',       '*`V myMIt{f{CHj?<t;qdz,:u(J2H650SCg^;/`F4pSNO3jxN,B!q|Mc^8N/s- |');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define('FS_METHOD', 'direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
set_time_limit(3000);
