<?php
/**
 * The template for displaying Category pages
 *
 * Used to display archive-type pages for posts in a category.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Nevara_Theme
 * @since Huge Shop 1.0
 */

$nevara_opt = get_option( 'nevara_opt' );

get_header();

$nevara_bloglayout = 'nosidebar';
if(isset($nevara_opt['blog_layout']) && $nevara_opt['blog_layout']!=''){
	$nevara_bloglayout = $nevara_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$nevara_bloglayout = $_GET['layout'];
}
$nevara_blogsidebar = 'right';
if(isset($nevara_opt['sidebarblog_pos']) && $nevara_opt['sidebarblog_pos']!=''){
	$nevara_blogsidebar = $nevara_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$nevara_blogsidebar = $_GET['sidebar'];
}
switch($nevara_bloglayout) {
	case 'sidebar':
		$nevara_blogclass = 'blog-sidebar';
		$nevara_blogcolclass = 9;
		Nevara_Class::nevara_post_thumbnail_size('nevara-category-thumb');
		break;
	case 'largeimage':
		$nevara_blogclass = 'blog-large';
		$nevara_blogcolclass = 9;
		$nevara_postthumb = '';
		break;
	default:
		$nevara_blogclass = 'blog-nosidebar';
		$nevara_blogcolclass = 12;
		$nevara_blogsidebar = 'none';
		Nevara_Class::nevara_post_thumbnail_size('nevara-post-thumb');
}
?>
<div class="main-container">
	<div class="title-breadcrumb">
		<div class="container">
			<div class="title-breadcrumb-inner">
				<header class="entry-header">
					<h1 class="entry-title"><?php if(isset($nevara_opt['blog_header_text']) && $nevara_opt['blog_header_text'] != "") { echo esc_attr($nevara_opt['blog_header_text']); } else { esc_html_e('Blog', 'nevara');}  ?></h1>
				</header>
				<?php Nevara_Class::nevara_breadcrumb(); ?>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			
			<?php if($nevara_blogsidebar=='left') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
			<div class="col-xs-12 <?php echo 'col-md-'.$nevara_blogcolclass; ?>">
			
				<div class="page-content blog-page <?php echo esc_attr($nevara_blogclass); if($nevara_blogsidebar=='left') {echo ' left-sidebar'; } if($nevara_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
				
					<?php if ( have_posts() ) : ?>
						<header class="archive-header">
							<h1 class="archive-title"><?php printf( esc_html__( 'Category Archives: %s', 'nevara' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>

						<?php if ( category_description() ) : // Show an optional category description ?>
							<div class="archive-meta"><?php echo category_description(); ?></div>
						<?php endif; ?>
						</header><!-- .archive-header -->

						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();

							/* Include the post format-specific template for the content. If you want to
							 * this in a child theme then include a file called called content-___.php
							 * (where ___ is the post format) and that will be used instead.
							 */
							get_template_part( 'content', get_post_format() );

						endwhile;
						?>
						
						<div class="pagination">
							<?php Nevara_Class::nevara_pagination(); ?>
						</div>
						
					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
				</div>
			</div>
			<?php if( $nevara_blogsidebar=='right') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
		
	</div> 
</div>

<?php get_footer(); ?>