<?php
function nevara_logo_shortcode( $atts ) {
	$nevara_opt = get_option( 'nevara_opt' );

	$atts = shortcode_atts( array(
							'logo_link' => 'yes',
							), $atts, 'roadlogo' );
	$html = '';

	if( isset($nevara_opt['logo_main']['url']) && $nevara_opt['logo_main']['url']!=''){
		$html .= '<div class="logo">';

			if($atts['logo_link']=='yes'){
				$html .= '<a href="'.esc_url( home_url( '/' ) ).'" title="'.esc_attr( get_bloginfo( 'name', 'display' ) ).'" rel="home">';
			}
				$html .= '<img src="'.esc_url($nevara_opt['logo_main']['url']).'" alt="'.esc_attr( get_bloginfo( 'name', 'display' ) ).'" />';

			if($atts['logo_link']=='yes'){
				$html .= '</a>';
			}

		$html .= '</div>';
	} else {
		$html .= '<h1 class="logo">';

		if($atts['logo_link']=='yes'){
			$html .= '<a href="'.esc_url( home_url( '/' ) ).'" title="'.esc_attr( get_bloginfo( 'name', 'display' ) ).'" rel="home">';
		}
		$html .= bloginfo( 'name' );

		if($atts['logo_link']=='yes'){
			$html .= '</a>';
		}

		$html .= '</h1>';
	}
	
	return $html;
}

function nevara_mainmenu_shortcode( $atts ) {
	$nevara_opt = get_option( 'nevara_opt' );

	$atts = shortcode_atts( array(
							'sticky_logoimage' => '',
							), $atts, 'roadmainmenu' );
	$html = '';
	
	ob_start(); ?>
	<div class="main-menu-wrapper">
		<div class="visible-small mobile-menu"> 
			<div class="mbmenu-toggler"><?php echo esc_attr($nevara_opt['mobile_menu_label']);?><span class="mbmenu-icon"><i class="fa fa-bars"></i></span></div>
			<div class="clearfix"></div>
			<?php wp_nav_menu( array( 'theme_location' => 'mobilemenu', 'container_class' => 'mobile-menu-container', 'menu_class' => 'nav-menu' ) ); ?>
		</div> 
		<div class="nav-container">
			<?php if( isset($atts['sticky_logoimage']) && $atts['sticky_logoimage']!=''){ ?>
				<div class="logo-sticky"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo  wp_get_attachment_url( $atts['sticky_logoimage']);?>" alt="" /></a></div>
			<?php } ?>
			<div class="horizontal-menu visible-large">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container_class' => 'primary-menu-container', 'menu_class' => 'nav-menu' ) ); ?>
			</div> 
		</div>  
	</div>	
	<?php
	$html .= ob_get_contents();

	ob_end_clean();
	
	return $html;
}

function nevara_roadcategoriesmenu_shortcode ( $atts ) {

	$nevara_opt = get_option( 'nevara_opt' );

	$html = '';

	ob_start();

	$cat_menu_class = '';

	if(isset($nevara_opt['categories_menu_home']) && $nevara_opt['categories_menu_home']) {
		$cat_menu_class .=' show_home';
	}
	if(isset($nevara_opt['categories_menu_sub']) && $nevara_opt['categories_menu_sub']) {
		$cat_menu_class .=' show_inner';
	}
	?>
	<div class="categories-menu visible-large <?php echo esc_attr($cat_menu_class); ?>">
		<div class="catemenu-toggler"><span><?php if(isset($nevara_opt)) { echo esc_attr($nevara_opt['categories_menu_label']); } else { echo esc_html('Category', 'nevara'); } ?></span><i class="fa fa-angle-down"></i></div>
		<?php wp_nav_menu( array( 'theme_location' => 'categories', 'container_class' => 'categories-menu-container', 'menu_class' => 'categories-menu' ) ); ?>
		<div class="morelesscate">
			<span class="morecate"><i class="fa fa-plus"></i><?php if ( isset($nevara_opt['categories_more_label']) && $nevara_opt['categories_more_label']!='' ) { echo esc_attr($nevara_opt['categories_more_label']); } else { echo esc_html('More Categories', 'nevara'); } ?></span>
			<span class="lesscate"><i class="fa fa-minus"></i><?php if ( isset($nevara_opt['categories_less_label']) && $nevara_opt['categories_less_label']!='' ) { echo esc_attr($nevara_opt['categories_less_label']); } else { echo esc_html('Close Menu', 'nevara'); } ?></span>
		</div>
	</div>
	<?php

	$html .= ob_get_contents();

	ob_end_clean();
	
	return $html;
}

function nevara_roadlangswitch_shortcode( $atts ) {
	$nevara_opt = get_option( 'nevara_opt' );

	$html = '';

	ob_start();

	if (class_exists('SitePress')) { ?>
		<div class="switcher">
			<div class="currency"><label><?php echo esc_html('Currency');?></label><?php do_action('currency_switcher'); ?></div>
			<div class="language"><label><?php echo esc_html('language');?></label><?php do_action('icl_language_selector'); ?></div> 
		</div> 
	<?php }

	$html .= ob_get_contents();

	ob_end_clean();
	
	return $html;
}

function nevara_roadsocialicons_shortcode( $atts ) {
	$nevara_opt = get_option( 'nevara_opt' );

	$html = '';

	ob_start();

	if(isset($nevara_opt['social_icons'])) {
		echo '<ul class="social-icons">';
		foreach($nevara_opt['social_icons'] as $key=>$value ) {
			if($value!=''){
				if($key=='vimeo'){
					echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>';
				} else {
					echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-'.esc_attr($key).'"></i></a></li>';
				}
			}
		}
		echo '</ul>';
	}

	$html .= ob_get_contents();

	ob_end_clean();
	
	return $html;
}

function nevara_roadminicart_shortcode( $atts ) {

	$html = '';

	ob_start();

	if ( class_exists( 'WC_Widget_Cart' ) ) {
		the_widget('Custom_WC_Widget_Cart');
	}

	$html .= ob_get_contents();

	ob_end_clean();
	
	return $html;
}

function nevara_roadproductssearch_shortcode( $atts ) {

	$html = '';

	ob_start();

	if( class_exists('WC_Widget_Product_Categories') && class_exists('WC_Widget_Product_Search') ) { ?>
		<div class="header-search">
			<div class="search-categories-container">
		  		<div class="cate-toggler"><?php esc_html_e('All Categories', 'nevara');?></div>
		  		<?php the_widget('WC_Widget_Product_Categories', array('hierarchical' => true, 'title' => 'All Categories', 'orderby' => 'order')); ?>
		  	</div> 
			<?php the_widget('WC_Widget_Product_Search', array('title' => 'Search')); ?>
		</div>
	<?php }

	$html .= ob_get_contents();

	ob_end_clean();
	
	return $html;
}

function nevara_brands_shortcode( $atts ) {
	global $nevara_opt;
	$brand_index = 0;
	
	if(isset($nevara_opt['brand_logos'])) {
		$brandfound = count($nevara_opt['brand_logos']);
	}
	$atts = shortcode_atts( array(
							'rowsnumber' => '1',
							'colsnumber' => '6',
							), $atts, 'ourbrands' );
	$html = '';
	
	if(isset($nevara_opt['brand_logos']) && $nevara_opt['brand_logos']) {
		$html .= '<div class="brands-carousel" data-col="'.$atts['colsnumber'].'">';
			foreach($nevara_opt['brand_logos'] as $brand) {
				if(is_ssl()){
					$brand['image'] = str_replace('http:', 'https:', $brand['image']);
				}
				$brand_index ++;
				if ( (0 == ( $brand_index - 1 ) % $atts['rowsnumber'] ) || $brand_index == 1) {
					$html .= '<div class="group">';
				}
				$html .= '<div>';
				$html .= '<a href="'.$brand['url'].'" title="'.$brand['title'].'">';
					$html .= '<img src="'.$brand['image'].'" alt="'.$brand['title'].'" />';
				$html .= '</a>';
				$html .= '</div>';
				if ( ( ( 0 == $brand_index % $atts['rowsnumber'] || $brandfound == $brand_index ))  ) {
					$html .= '</div>';
				}
			}
		$html .= '</div>';
	}
	
	return $html;
}

function nevara_counter_shortcode( $atts ) {
	
	$atts = shortcode_atts( array(
							'image' => '',
							'number' => '100',
							'text' => 'Demo text',
							), $atts, 'nevara_counter' );
	$html = '';
	$html.='<div class="nevara-counter">';
		$html.='<div class="counter-image">';
			$html.='<img src="'.wp_get_attachment_url($atts['image']).'" alt="" />';
		$html.='</div>';
		$html.='<div class="counter-info">';
			$html.='<div class="counter-number">';
				$html.='<span>'.$atts['number'].'</span>';
			$html.='</div>';
			$html.='<div class="counter-text">';
				$html.='<span>'.$atts['text'].'</span>';
			$html.='</div>';
		$html.='</div>';
	$html.='</div>';
	
	return $html;
}

function nevara_list_categories_shortcode( $atts ) {

	$atts = shortcode_atts( array(
		'category' => '', 
	), $atts, 'list_categories' );
	
	$html = '';
	
	$html .= '<div class="category-wrapper">';
		$pcategory = get_term_by( 'slug', $atts['category'], 'product_cat', 'ARRAY_A' );
		if($pcategory){
			$html .= '<div class="category-list">';
				$html .= '<h3><a href="'. get_term_link($pcategory['slug'], 'product_cat') .'">'. $pcategory['name'] .'</a></h3>';
				
				$html .= '<ul>';
					$args2 = array(
						'taxonomy'     => 'product_cat',
						'child_of'     => 0,
						'parent'       => $pcategory['term_id'],
						'orderby'      => 'name',
						'show_count'   => 0,
						'pad_counts'   => 0,
						'hierarchical' => 0,
						'title_li'     => '',
						'hide_empty'   => 0
					);
					$sub_cats = get_categories( $args2 );

					if($sub_cats) {
						foreach($sub_cats as $sub_category) {
							$html .= '<li><a href="'.get_term_link($sub_category->slug, 'product_cat').'">'.$sub_category->name.'</a></li>';
						}
					}
					$html .= '<li class="view-all"><a href="'. get_term_link($pcategory['slug'], 'product_cat') .'">'. esc_html('View all') .'</a></li>';
				$html .= '</ul>';
			$html .= '</div>'; 
		}
	$html .= '</div>';
	
	return $html;
}

function nevara_roadtimesale_shortcode( $atts ) {  
	$nevara_opt = get_option( 'nevara_opt' );

	$atts = shortcode_atts( array(
							'sale-date-time' => '',
							), $atts, 'roadtimesale' );

	$html = '';

	ob_start();
	if(isset($nevara_opt['sale-date-time'])) {
		echo '<div class="countdownsale" data-date="'.$nevara_opt['sale-date-time'].' 00:00:00" style="width: 470px; height: 117px;"></div>';
	}
	$html .= ob_get_contents();

	ob_end_clean();
	
	return $html;
}

function nevara_categoriescarousel_shortcode( $atts ) {
	global $nevara_opt;
	$categories_index = 0;
	if(isset($nevara_opt['cate_images'])){
		$categoriesfound = count($nevara_opt['cate_images']);
	}
	
	$atts = shortcode_atts( array(
							'rowsnumber' => '1',
							'colsnumber' => '6',
							), $atts, 'categoriescarousel' );
	$html = '';
	
	if(isset($nevara_opt['cate_images'])){
		$html .= '<div class="categories-carousel" data-col="'.$atts['colsnumber'].'">';
			foreach($nevara_opt['cate_images'] as $categories) {
				if(is_ssl()){
					$categories['image'] = str_replace('http:', 'https:', $categories['image']);
				}
				$categories_index ++;
				if ( (0 == ( $categories_index - 1 ) % $atts['rowsnumber'] ) || $categories_index == 1) {
					$html .= '<div class="group">';
				}
				$html .= '<div class="item-inner">';
					$html .= '<div class="description">';
						$html .= '<h3 class="title">'.$categories['title'].'</h3>';
						$html .= '<div class="des">'.$categories['description'].'</div>';
					$html .= '</div>';
					$html .= '<a href="'.$categories['url'].'" class="image" title="'.$categories['title'].'">';
						$html .= '<img src="'.$categories['image'].'" alt="'.$categories['title'].'" />';
					$html .= '</a>'; 
				$html .= '</div>';
				if ( ( ( 0 == $categories_index % $atts['rowsnumber'] || $categoriesfound == $categories_index ))  ) {
					$html .= '</div>';
				}
			}
		$html .= '</div>';
	}
	
	return $html;
}

function nevara_latestposts_shortcode( $atts ) {
	global $nevara_opt;
	$post_index = 0;
	$atts = shortcode_atts( array(
		'posts_per_page' => 5,
		'order' => 'DESC',
		'orderby' => 'post_date',
		'image' => 'wide', //square
		'length' => 20,
		'rowsnumber' => '1',
		'colsnumber' => '4',
		'image1' => 'square',
	), $atts, 'latestposts' );
	
	if($atts['image']=='wide'){
		$imagesize = 'nevara-post-thumbwide';
	} else {
		$imagesize = 'nevara-post-thumb';
	}
	$html = '';

	$postargs = array(
		'posts_per_page'   => $atts['posts_per_page'],
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => $atts['orderby'],
		'order'            => $atts['order'],
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'post',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'post_status'      => 'publish',
		'suppress_filters' => true );
	
	$postslist = get_posts( $postargs );

	$html.='<div class="posts-carousel" data-col="'.$atts['colsnumber'].'">';

			foreach ( $postslist as $post ) {
				$post_index ++;
				if ( (0 == ( $post_index - 1 ) % $atts['rowsnumber'] ) || $post_index == 1) {
					$html .= '<div class="group">';
				} 
				$html.='<div class="item-col">';
					$html.='<div class="post-wrapper">';

					// author link
					$author_id = $post->post_author;
					$author_url = get_author_posts_url( get_the_author_meta( 'ID', $author_id ) );
					$author_name = get_the_author_meta( 'user_nicename', $author_id );
					
					//comment variables
					$num_comments = (int)get_comments_number($post->ID);
					$write_comments = '';
					if ( comments_open($post->ID) ) {
						if ( $num_comments == 0 ) {
							$comments = wp_kses(__('<span>0</span> comments', 'nevara'),array('span'=>array()));
						} elseif ( $num_comments > 1 ) {
							$comments = '<span>'.$num_comments .'</span>'. esc_html__(' comments', 'nevara');
						} else {
							$comments = wp_kses(__('<span>1</span> comment', 'nevara'),array('span'=>array()));
						}
						$write_comments = '<a href="' . get_comments_link($post->ID) .'">'.$comments.'</a>';
					}
					// Read more text
					if(!isset($nevara_opt['readmore_text'])){
						$nevara_opt['readmore_text'] = 'Read more';
					}
					
					$html.='<div class="post-thumb">'; 
						$html.='<a href="'.get_the_permalink($post->ID).'">'.get_the_post_thumbnail($post->ID, $imagesize).'</a>'; 
					$html.='</div>';
					
					$html.='<div class="post-info">';   
						$html.='<h3 class="post-title"><a href="'.get_the_permalink($post->ID).'">'.get_the_title($post->ID).'</a></h3>';	 
						$html.='<div class="post-excerpt">';
							$html.= Nevara_Class::nevara_excerpt_by_id($post, $length = $atts['length']);
						$html.='</div>';
						$html.='<div class="post-meta">';   
							$html.='<p class="post-author">';
								$html.= sprintf( wp_kses(__( '%s', 'nevara' ), array('a'=>array('href'=>array()))), esc_html__('By: ', 'nevara').'<a href="'.$author_url.'">'.$author_name.'</a>' );
							$html.='</p>'; 

							$html.='<span class="post-slash"> - </span>';
							$html.='<p class="post-comment">'.$comments.'</p>';
							$html.='<div class="post-date"><div class="post-date-inner"><span class="day">'.get_the_date('d', $post->ID).'</span> <span class="month">' .get_the_date('M', $post->ID).',</span> <span class="year">' .get_the_date('Y', $post->ID).'</span></div></div>';
						$html.='</div>';
						
						$html.='<a class="readmore" href="'.get_the_permalink($post->ID).'">'.'<span>' .esc_attr($nevara_opt['readmore_text']). '</span>'.'</a>';

					$html.='</div>';

				$html.='</div>';
			$html.='</div>';
			if ( ( ( 0 == $post_index % $atts['rowsnumber'] || $atts['posts_per_page'] == $post_index ))  ) {
				$html .= '</div>';
			}
		}
	$html.='</div>';

	wp_reset_postdata();
	
	return $html;
}

function nevara_contact_map( $atts ) {
	global $nevara_mapid;
	
	if(!isset($nevara_mapid)){
		$nevara_mapid = 1;
	} else {
		$nevara_mapid++;
	}
	$atts = shortcode_atts( array(
		'map_height' => 400,
		'map_zoom' => 17,
		'lat1' => '',
		'long1' => '',
		'address1' => '',
		'marker1' => '',
		'description1' => '',
		'lat2' => '',
		'long2' => '',
		'address2' => '',
		'marker2' => '',
		'description2' => '',
		'lat3' => '',
		'long3' => '',
		'address3' => '',
		'marker3' => '',
		'description3' => '',
		'lat4' => '',
		'long4' => '',
		'address4' => '',
		'marker4' => '',
		'description4' => '',
		'lat5' => '',
		'long5' => '',
		'address5' => '',
		'marker5' => '',
		'description5' => '',
		
	), $atts, 'nevara_map' );
	
	$map_zoom = 17;
	if(intval($atts['map_zoom'])){
		$map_zoom = intval($atts['map_zoom']);
	}
	$map_height = 400;
	if(intval($atts['map_height'])){
		$map_height = intval($atts['map_height']);
	}
	
	$markers = array(
		array(
			'lat1' => $atts['lat1'],
			'long1' => $atts['long1'],
			'address1' => $atts['address1'],
			'marker1' => wp_get_attachment_url($atts['marker1']),
			'description1' => $atts['description1'],
		),
		array(
			'lat2' => $atts['lat2'],
			'long2' => $atts['long2'],
			'address2' => $atts['address2'],
			'marker2' => wp_get_attachment_url($atts['marker2']),
			'description2' => $atts['description2'],
		),
		array(
			'lat3' => $atts['lat3'],
			'long3' => $atts['long3'],
			'address3' => $atts['address3'],
			'marker3' => wp_get_attachment_url($atts['marker3']),
			'description3' => $atts['description3'],
		),
		array(
			'lat4' => $atts['lat4'],
			'long4' => $atts['long4'],
			'address4' => $atts['address4'],
			'marker4' => wp_get_attachment_url($atts['marker4']),
			'description4' => $atts['description4'],
		),
		array(
			'lat5' => $atts['lat5'],
			'long5' => $atts['long5'],
			'address5' => $atts['address5'],
			'marker5' => wp_get_attachment_url($atts['marker5']),
			'description5' => $atts['description5'],
		),
	);
	
	$html = '';
	
	$html.='<div class="map-wrapper">';
		$html.='<div id="map'.$nevara_mapid.'" class="map" style="height: '.$map_height.'px"></div>';
	$html.='</div>';
	
	//Add google map API
	wp_enqueue_script( 'gmap-api-js', 'http://maps.google.com/maps/api/js?sensor=false' , array(), '3', false );
	// Add jquery.gmap.js file
	wp_enqueue_script( 'jquery.gmap-js', get_template_directory_uri() . '/js/jquery.gmap.js', array(), '2.1.5', false );
	wp_enqueue_script( 'shortcodes.js', get_template_directory_uri() . '/js/shortcodes.js', array(), '1.0.0', false );
	$theme_uri = get_template_directory_uri();
	wp_localize_script('shortcodes.js','data',array(
		'theme_uri'=>$theme_uri,
		'markers'=>$markers,
		'map_id'=>$nevara_mapid,
		'map_zoom'=>$map_zoom,
	));
	return $html;
}
?>