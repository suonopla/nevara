<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Nevara_Theme
 * @since Huge Shop 1.0
 */

$nevara_opt = get_option( 'nevara_opt' );

get_header();

$nevara_bloglayout = 'sidebar';

if(isset($nevara_opt['blog_layout']) && $nevara_opt['blog_layout']!=''){
	$nevara_bloglayout = $nevara_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$nevara_bloglayout = $_GET['layout'];
}
$nevara_blogsidebar = 'right';
if(isset($nevara_opt['sidebarblog_pos']) && $nevara_opt['sidebarblog_pos']!=''){
	$nevara_blogsidebar = $nevara_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$nevara_blogsidebar = $_GET['sidebar'];
}

switch($nevara_bloglayout) {
	case 'sidebar':
		$nevara_blogclass = 'blog-sidebar';
		$nevara_blogcolclass = 9;
		Nevara_Class::nevara_post_thumbnail_size('nevara-category-thumb');
		break;
	case 'largeimage':
		$nevara_blogclass = 'blog-large';
		$nevara_blogcolclass = 9;
		Nevara_Class::nevara_post_thumbnail_size('nevara-category-thumb');
		break;
	case 'grid':
		$nevara_blogclass = 'grid';
		$nevara_blogcolclass = 9;
		Nevara_Class::nevara_post_thumbnail_size('nevara-category-thumb');
		break;
	default:
		$nevara_blogclass = 'blog-nosidebar';
		$nevara_blogcolclass = 12;
		$nevara_blogsidebar = 'none';
		Nevara_Class::nevara_post_thumbnail_size('nevara-post-thumb');
}
?>

<div class="main-container"> 
	<div class="title-breadcrumb">
		<div class="container">
			<div class="title-breadcrumb-inner">
				<header class="entry-header">
					<h1 class="entry-title"><?php if(isset($nevara_opt)) { echo esc_attr($nevara_opt['blog_header_text']); } else { esc_html_e('Blog', 'nevara');}  ?></h1>
				</header>
				<?php Nevara_Class::nevara_breadcrumb(); ?>
			</div>
		</div>
	</div>
	<div class="container">
		
		<div class="row">
			<div class="col-xs-12 <?php echo 'col-md-'.$nevara_blogcolclass; ?> <?php echo ($nevara_blogsidebar=='left') ? 'pull-right':'pull-left'?>">
				<div class="page-content blog-page <?php echo esc_attr($nevara_blogclass); if($nevara_blogsidebar=='left') {echo ' left-sidebar'; } if($nevara_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<?php if ( have_posts() ) : ?>

						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
							
							<?php get_template_part( 'content', get_post_format() ); ?>
							
						<?php endwhile; ?>

						<div class="pagination">
							<?php Nevara_Class::nevara_pagination(); ?>
						</div>
						
					<?php else : ?>

						<article id="post-0" class="post no-results not-found">

						<?php if ( current_user_can( 'edit_posts' ) ) :
							// Show a different message to a logged-in user who can add posts.
						?>
							<header class="entry-header">
								<h1 class="entry-title"><?php esc_html_e( 'No posts to display', 'nevara' ); ?></h1>
							</header>

							<div class="entry-content">
								<p><?php printf( wp_kses(__( 'Ready to publish your first post? <a href="%s">Get started here</a>.', 'nevara' ), array('a'=>array('href'=>array()))), admin_url( 'post-new.php' ) ); ?></p>
							</div><!-- .entry-content -->

						<?php else :
							// Show the default message to everyone else.
						?>
							<header class="entry-header">
								<h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'nevara' ); ?></h1>
							</header>

							<div class="entry-content">
								<p><?php esc_html_e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'nevara' ); ?></p>
								<?php get_search_form(); ?>
							</div><!-- .entry-content -->
						<?php endif; // end current_user_can() check ?>

						</article><!-- #post-0 -->

					<?php endif; // end have_posts() check ?>
				</div>
				
			</div>
			<?php if( $nevara_blogsidebar != 'none') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
	</div> 
</div>
<?php get_footer(); ?>