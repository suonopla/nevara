jQuery(document).ready(function(){

	var $theme_uri = data.theme_uri;

	var $opt = data.opt;

	var $map_desc = data.map_desc;

	var markers = [];
	var temp = {};
	if($opt['address_by']=='coordinate'){
		temp = {
			latitude: $opt['map_lat'],
			longitude: $opt['map_long'],
			html: $map_desc,
			popup: true
		}
		if(typeof $opt['map_marker']['url'] != 'undefined' && $opt['map_marker']['url']!=''){
			temp['icon'] = {
				image: $opt['map_marker']['url'],
				iconsize: [40, 46],
				iconanchor: [40, 46]
			}
		}else{
			temp['icon'] = {
				image: $theme_uri+'/images/marker.png',
				iconsize: [40, 46],
				iconanchor: [40, 46]
			}
		}

	}else{

		temp = {
			address: $opt['map_address'],
			html: $map_desc,
			popup: true
		}
		if(typeof $opt['map_marker']['url'] != 'undefined' && $opt['map_marker']['url']!=''){
			temp['icon'] = {
				image: $opt['map_marker']['url'],
				iconsize: [40, 46],
				iconanchor: [40, 46]
			}
		}else{
			temp['icon'] = {
				image: $theme_uri+'/images/marker.png',
				iconsize: [40, 46],
				iconanchor: [40, 46]
			}
		}
	}
	markers = [temp];
	var options = {

		scrollwheel: false,

		zoom:$opt['map_zoom'],

		markers:markers

	};

	if($opt['address_by']=='address'){

		options['address']=$opt['map_address'];

	}

	jQuery('#map').gMap(options);

});