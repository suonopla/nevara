var yith_magnifier_options = {
    enableSlider: data.enableSlider,
    showTitle: false,
    zoomWidth: data.zoomWidth,
    zoomHeight: data.zoomHeight,
    position: data.position,
    lensOpacity: data.lensOpacity,
    softFocus: data.softFocus,
    adjustY: 0,
    disableRightClick: false,
    phoneBehavior: data.phoneBehavior,
    loadingLabel: data.loadingLabel
};
if (data.enableSlider) {
    yith_magnifier_options['sliderOptions'] = {
        responsive: data.responsive,
        circular: data.circular,
        infinite: data.infinite,
        direction: 'up',
        debug: false,
        auto: false,
        align: 'left',
        height: "100%", //turn vertical
        width:110,
        prev: {
            button: "#slider-prev",
            key: "left"
        },
        next: {
            button: "#slider-next",
            key: "right"
        },
        scroll: {
            items: 1,
            pauseOnHover: true
        },
        items: {
            visible: parseInt(data.visible)
        },
        swipe: {
            onTouch: true,
            onMouse: true
        },
        mousewheel: {
            items: 1
        }
    }
}