jQuery(document).ready(function(){

	var $map_id = data.map_id;

	var $map_zoom = data.map_zoom;

	var $markers = data.markers;

	var $theme_uri = data.theme_uri;

	var $markeridx = 0;

	var $markers_map = [];

	for(var i in $markers){

		$markeridx++;

		$marker = $markers[i];



		$map_desc = $marker['description'+$markeridx].replace(/\r\n|\r|\n/gi, '');

		$map_desc = $map_desc.replace(/&/g, '&amp;')

             .replace(/'/g, '&apos;')

             .replace(/"/g, '&quot;')

             .replace(/\\/g, '\\\\')

             .replace(/</g, '&lt;')

             .replace(/>/g, '&gt;').replace(/\u0000/g, '\\0');

        var temp = {

			popup: true

        }

        if((typeof $marker['marker'+$markeridx] != 'undefined' && $marker['marker'+$markeridx]!='')){

        	temp['icon'] = {

        		image:$marker['marker'+$markeridx],

        		iconsize: [40, 46],

				iconanchor: [40, 40]

        	}

        }else{
        	temp['icon'] = {
				image: $theme_uri+'/images/marker.png',
				iconsize: [40, 46],
				iconanchor: [40, 46]
			}
        }

		if( $marker['address'+$markeridx]!='' || ($marker['lat'+$markeridx]!='' && $marker['long'+$markeridx]!='') ){

			if($marker['address'+$markeridx]!=''){

				temp['address']= $marker['address'+$markeridx];

			}else{

				temp['latitude']=$marker['lat'+$markeridx];

				temp['longitude']=$marker['long'+$markeridx];

			}

			temp['html'] = $map_desc;

		}

		$markers_map.push(temp);

	}

	var options = {

		scrollwheel: false,

		zoom: parseInt($map_zoom),

		markers:$markers_map

	}



	jQuery('#map'+$map_id).gMap(options);

});