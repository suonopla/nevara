# Copyright (C) 2014 the WordPress team
# This file is distributed under the GNU General Public License v2 or later.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Nevara 1.0\n"
"Report-Msgid-Bugs-To: http://wordpress.org/tags/roadthemes\n"
"POT-Creation-Date: 2017-10-20 16:52+0700\n"
"PO-Revision-Date: 2017-09-27 10:37+0700\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.0.4\n"
"X-Poedit-KeywordsList: _e;_n;__;_x;_ex;esc_html__;esc_html_e\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SearchPath-0: .\n"

#: 404.php:18
msgid "404"
msgstr ""

#: 404.php:19
msgid "Oops! PAGE NOT BE FOUND"
msgstr ""

#: 404.php:20
msgid ""
"Sorry but the page you are looking for does not exist, have been removed, "
"name changed or is temporarity unavailable."
msgstr ""

#: 404.php:22
msgid "Back to home"
msgstr ""

#: 404.php:22
msgid "Back to home page"
msgstr ""

#: archive.php:61 author.php:55 category.php:55 image.php:48 index.php:66
#: search.php:51 single.php:44 tag.php:55 theme-config.php:1167
msgid "Blog"
msgstr ""

#: author.php:81
#, php-format
msgid "Author Archives: %s"
msgstr ""

#: author.php:110
#, php-format
msgid "About %s"
msgstr ""

#: category.php:73
#, php-format
msgid "Category Archives: %s"
msgstr ""

#: class-tgm-plugin-activation.php:327 functions.php:1659
msgid "Install Required Plugins"
msgstr ""

#: class-tgm-plugin-activation.php:328 functions.php:1660
msgid "Install Plugins"
msgstr ""

#: class-tgm-plugin-activation.php:330 functions.php:1661
#, php-format
msgid "Installing Plugin: %s"
msgstr ""

#: class-tgm-plugin-activation.php:332
#, php-format
msgid "Updating Plugin: %s"
msgstr ""

#: class-tgm-plugin-activation.php:333 functions.php:1662
msgid "Something went wrong with the plugin API."
msgstr ""

#: class-tgm-plugin-activation.php:385 functions.php:1673
msgid "Return to Required Plugins Installer"
msgstr ""

#: class-tgm-plugin-activation.php:386 class-tgm-plugin-activation.php:827
#: class-tgm-plugin-activation.php:2533 class-tgm-plugin-activation.php:3580
msgid "Return to the Dashboard"
msgstr ""

#: class-tgm-plugin-activation.php:387 class-tgm-plugin-activation.php:3159
#: functions.php:1674
msgid "Plugin activated successfully."
msgstr ""

#: class-tgm-plugin-activation.php:388 class-tgm-plugin-activation.php:2952
msgid "The following plugin was activated successfully:"
msgstr ""

#: class-tgm-plugin-activation.php:390
#, php-format
msgid "No action taken. Plugin %1$s was already active."
msgstr ""

#: class-tgm-plugin-activation.php:392
#, php-format
msgid ""
"Plugin not activated. A higher version of %s is needed for this theme. "
"Please update the plugin."
msgstr ""

#: class-tgm-plugin-activation.php:394
#, php-format
msgid "All plugins installed and activated successfully. %1$s"
msgstr ""

#: class-tgm-plugin-activation.php:395
msgid "Dismiss this notice"
msgstr ""

#: class-tgm-plugin-activation.php:396
msgid ""
"There are one or more required or recommended plugins to install, update or "
"activate."
msgstr ""

#: class-tgm-plugin-activation.php:397
msgid "Please contact the administrator of this site for help."
msgstr ""

#: class-tgm-plugin-activation.php:522
msgid "This plugin needs to be updated to be compatible with your theme."
msgstr ""

#: class-tgm-plugin-activation.php:523
msgid "Update Required"
msgstr ""

#: class-tgm-plugin-activation.php:934
msgid ""
"The remote plugin package does not contain a folder with the desired slug "
"and renaming did not work."
msgstr ""

#: class-tgm-plugin-activation.php:934 class-tgm-plugin-activation.php:937
msgid ""
"Please contact the plugin provider and ask them to package their plugin "
"according to the WordPress guidelines."
msgstr ""

#: class-tgm-plugin-activation.php:937
msgid ""
"The remote plugin package consists of more than one file, but the files are "
"not packaged in a folder."
msgstr ""

#: class-tgm-plugin-activation.php:1982
#, php-format
msgid "TGMPA v%s"
msgstr ""

#: class-tgm-plugin-activation.php:2273
msgid "Required"
msgstr ""

#: class-tgm-plugin-activation.php:2276
msgid "Recommended"
msgstr ""

#: class-tgm-plugin-activation.php:2292
msgid "WordPress Repository"
msgstr ""

#: class-tgm-plugin-activation.php:2295
msgid "External Source"
msgstr ""

#: class-tgm-plugin-activation.php:2298
msgid "Pre-Packaged"
msgstr ""

#: class-tgm-plugin-activation.php:2315
msgid "Not Installed"
msgstr ""

#: class-tgm-plugin-activation.php:2319
msgid "Installed But Not Activated"
msgstr ""

#: class-tgm-plugin-activation.php:2321
msgid "Active"
msgstr ""

#: class-tgm-plugin-activation.php:2327
msgid "Required Update not Available"
msgstr ""

#: class-tgm-plugin-activation.php:2330
msgid "Requires Update"
msgstr ""

#: class-tgm-plugin-activation.php:2333
msgid "Update recommended"
msgstr ""

#: class-tgm-plugin-activation.php:2342
#, php-format
msgid "%1$s, %2$s"
msgstr ""

#: class-tgm-plugin-activation.php:2392
#, php-format
msgid "To Install <span class=\"count\">(%s)</span>"
msgstr ""

#: class-tgm-plugin-activation.php:2396
#, php-format
msgid "Update Available <span class=\"count\">(%s)</span>"
msgstr ""

#: class-tgm-plugin-activation.php:2400
#, php-format
msgid "To Activate <span class=\"count\">(%s)</span>"
msgstr ""

#: class-tgm-plugin-activation.php:2482
msgid "unknown"
msgstr ""

#: class-tgm-plugin-activation.php:2490
msgid "Installed version:"
msgstr ""

#: class-tgm-plugin-activation.php:2498
msgid "Minimum required version:"
msgstr ""

#: class-tgm-plugin-activation.php:2510
msgid "Available version:"
msgstr ""

#: class-tgm-plugin-activation.php:2533
msgid "No plugins to install, update or activate."
msgstr ""

#: class-tgm-plugin-activation.php:2547
msgid "Plugin"
msgstr ""

#: class-tgm-plugin-activation.php:2548
msgid "Source"
msgstr ""

#: class-tgm-plugin-activation.php:2549
msgid "Type"
msgstr ""

#: class-tgm-plugin-activation.php:2553
msgid "Version"
msgstr ""

#: class-tgm-plugin-activation.php:2554
msgid "Status"
msgstr ""

#: class-tgm-plugin-activation.php:2603
#, php-format
msgid "Install %2$s"
msgstr ""

#: class-tgm-plugin-activation.php:2608
#, php-format
msgid "Update %2$s"
msgstr ""

#: class-tgm-plugin-activation.php:2614
#, php-format
msgid "Activate %2$s"
msgstr ""

#: class-tgm-plugin-activation.php:2684
msgid "Upgrade message from the plugin author:"
msgstr ""

#: class-tgm-plugin-activation.php:2717
msgid "Install"
msgstr ""

#: class-tgm-plugin-activation.php:2723
msgid "Update"
msgstr ""

#: class-tgm-plugin-activation.php:2726
msgid "Activate"
msgstr ""

#: class-tgm-plugin-activation.php:2757
msgid "No plugins were selected to be installed. No action taken."
msgstr ""

#: class-tgm-plugin-activation.php:2759
msgid "No plugins were selected to be updated. No action taken."
msgstr ""

#: class-tgm-plugin-activation.php:2800
msgid "No plugins are available to be installed at this time."
msgstr ""

#: class-tgm-plugin-activation.php:2802
msgid "No plugins are available to be updated at this time."
msgstr ""

#: class-tgm-plugin-activation.php:2908
msgid "No plugins were selected to be activated. No action taken."
msgstr ""

#: class-tgm-plugin-activation.php:2934
msgid "No plugins are available to be activated at this time."
msgstr ""

#: class-tgm-plugin-activation.php:3158
msgid "Plugin activation failed."
msgstr ""

#: class-tgm-plugin-activation.php:3498
#, php-format
msgid "Updating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: class-tgm-plugin-activation.php:3501
#, php-format
msgid "An error occurred while installing %1$s: <strong>%2$s</strong>."
msgstr ""

#: class-tgm-plugin-activation.php:3503
#, php-format
msgid "The installation of %1$s failed."
msgstr ""

#: class-tgm-plugin-activation.php:3507
msgid ""
"The installation and activation process is starting. This process may take a "
"while on some hosts, so please be patient."
msgstr ""

#: class-tgm-plugin-activation.php:3509
#, php-format
msgid "%1$s installed and activated successfully."
msgstr ""

#: class-tgm-plugin-activation.php:3509 class-tgm-plugin-activation.php:3517
msgid "Show Details"
msgstr ""

#: class-tgm-plugin-activation.php:3509 class-tgm-plugin-activation.php:3517
msgid "Hide Details"
msgstr ""

#: class-tgm-plugin-activation.php:3510
msgid "All installations and activations have been completed."
msgstr ""

#: class-tgm-plugin-activation.php:3512
#, php-format
msgid "Installing and Activating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: class-tgm-plugin-activation.php:3515
msgid ""
"The installation process is starting. This process may take a while on some "
"hosts, so please be patient."
msgstr ""

#: class-tgm-plugin-activation.php:3517
#, php-format
msgid "%1$s installed successfully."
msgstr ""

#: class-tgm-plugin-activation.php:3518
msgid "All installations have been completed."
msgstr ""

#: class-tgm-plugin-activation.php:3520
#, php-format
msgid "Installing Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: comments.php:32 content-audio.php:120 content-gallery.php:120
#: content-image.php:121 content-video.php:118 content.php:121
#: functions.php:1449
msgid "1 comment"
msgstr ""

#: comments.php:52
msgid "Comments are closed."
msgstr ""

#: content-audio.php:29 content-audio.php:44 content-audio.php:82
#: content-gallery.php:28 content-gallery.php:44 content-gallery.php:83
#: content-image.php:30 content-image.php:45 content-image.php:84
#: content-video.php:28 content-video.php:43 content-video.php:81
#: content.php:30 content.php:45 content.php:84
msgid "Posted by"
msgstr ""

#: content-audio.php:97 content-gallery.php:98 content-image.php:99
#: content-video.php:96 content.php:99
msgid "Continue reading <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: content-audio.php:98 content-gallery.php:99 content-image.php:100
#: content-page.php:20 content-video.php:97 content.php:100 image.php:122
msgid "Pages:"
msgstr ""

#: content-audio.php:104 content-gallery.php:104 content-image.php:105
#: content-video.php:102 content.php:105
msgid "Read more"
msgstr ""

#: content-audio.php:116 content-gallery.php:116 content-image.php:117
#: content-video.php:114 content.php:117 functions.php:1445
msgid "0 comments"
msgstr ""

#: content-audio.php:118 content-gallery.php:118 content-image.php:119
#: content-video.php:116 content.php:119 functions.php:1447 functions.php:1496
#: include/shortcodes.php:424
msgid " comments"
msgstr ""

#: content-audio.php:146 content-gallery.php:146 content-image.php:147
#: content-video.php:144 content.php:147
msgid "About the Author:"
msgstr ""

#: content-audio.php:158 content-gallery.php:159 content-image.php:161
#: content-video.php:157 content.php:160
msgid "Related posts"
msgstr ""

#: content-gallery.php:33 content-gallery.php:49
msgid "On"
msgstr ""

#: content-none.php:13 index.php:109 search.php:85
msgid "Nothing Found"
msgstr ""

#: content-none.php:17 index.php:113
msgid ""
"Apologies, but no results were found. Perhaps searching will help find a "
"related post."
msgstr ""

#: functions.php:237
msgid "Search..."
msgstr ""

#: functions.php:256
msgid "Search product..."
msgstr ""

#: functions.php:411
msgid "Primary Menu"
msgstr ""

#: functions.php:412
msgid "Top Menu"
msgstr ""

#: functions.php:413
msgid "Mobile Menu"
msgstr ""

#: functions.php:414 include/map_shortcodes.php:48 theme-config.php:507
msgid "Categories Menu"
msgstr ""

#: functions.php:914
msgid "Blog Sidebar"
msgstr ""

#: functions.php:916
msgid "Sidebar on blog page"
msgstr ""

#: functions.php:924
msgid "Shop Sidebar"
msgstr ""

#: functions.php:926
msgid "Sidebar on shop page (only sidebar shop layout)"
msgstr ""

#: functions.php:934
msgid "Pages Sidebar"
msgstr ""

#: functions.php:936
msgid "Sidebar on content pages"
msgstr ""

#: functions.php:973
msgid ""
"This content will be used to replace the featured image, use shortcode here"
msgstr ""

#: functions.php:994
msgid "Select a custom sidebar for this post/page"
msgstr ""

#: functions.php:1013
msgid "Sidebar position"
msgstr ""

#: functions.php:1030
msgid "Post featured content"
msgstr ""

#: functions.php:1036 functions.php:1044
msgid "Custom Sidebar"
msgstr ""

#: functions.php:1205 functions.php:1206 functions.php:1207
#, php-format
msgid "Archive for: %s"
msgstr ""

#: functions.php:1206
msgid "F Y"
msgstr ""

#: functions.php:1207
msgid "Y"
msgstr ""

#: functions.php:1208
msgid "Archive for"
msgstr ""

#: functions.php:1209
msgid "Blog Archives"
msgstr ""

#: functions.php:1210
msgid "Search Results"
msgstr ""

#: functions.php:1294 functions.php:1306 functions.php:1307 functions.php:1308
msgid "on"
msgstr ""

#: functions.php:1343 single.php:79
msgid "Post navigation"
msgstr ""

#: functions.php:1344
msgid "<span class=\"meta-nav\">&larr;</span> Older posts"
msgstr ""

#: functions.php:1345
msgid "Newer posts <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: functions.php:1360
msgid "Previous"
msgstr ""

#: functions.php:1361
msgid "Next"
msgstr ""

#: functions.php:1382
msgid "Pingback:"
msgstr ""

#: functions.php:1382
msgid "(Edit)"
msgstr ""

#: functions.php:1401
msgid "Post author"
msgstr ""

#: functions.php:1406
#, php-format
msgid "%1$s at %2$s"
msgstr ""

#: functions.php:1410
msgid "Reply"
msgstr ""

#: functions.php:1414
msgid "Your comment is awaiting moderation."
msgstr ""

#: functions.php:1419 image.php:149
msgid "Edit"
msgstr ""

#: functions.php:1454
#, php-format
msgid "%1$s "
msgstr ""

#: functions.php:1459
msgid " / "
msgstr ""

#: functions.php:1461
#, php-format
msgid "Tags: %1$s"
msgstr ""

#: functions.php:1472
#, php-format
msgid "View all posts by %s"
msgstr ""

#: functions.php:1476
#, php-format
msgid "Posted by %1$s / %2$s"
msgstr ""

#: functions.php:1494 include/shortcodes.php:422
msgid "<span>0</span> comments"
msgstr ""

#: functions.php:1498 include/shortcodes.php:426
msgid "<span>1</span> comment"
msgstr ""

#: functions.php:1503
#, php-format
msgid "%1$s"
msgstr ""

#: functions.php:1675
#, php-format
msgid "All plugins installed and activated successfully. %s"
msgstr ""

#: image.php:138
msgid "Published"
msgstr ""

#: image.php:138
msgid "Link to full-size image"
msgstr ""

#: image.php:138
#, php-format
msgid "Return to %7$s"
msgstr ""

#: image.php:159
msgid "&larr; Previous"
msgstr ""

#: image.php:160
msgid "Next &rarr;"
msgstr ""

#: include/map_shortcodes.php:9
msgid "Logo"
msgstr ""

#: include/map_shortcodes.php:12 include/map_shortcodes.php:33
#: include/map_shortcodes.php:51 include/map_shortcodes.php:61
#: include/map_shortcodes.php:71 include/map_shortcodes.php:79
#: include/map_shortcodes.php:89 include/map_shortcodes.php:99
#: include/map_shortcodes.php:118 include/map_shortcodes.php:149
#: include/map_shortcodes.php:167 include/map_shortcodes.php:198
#: include/map_shortcodes.php:217 include/map_shortcodes.php:275
#: include/map_shortcodes.php:345 include/map_shortcodes.php:363
#: include/map_shortcodes.php:397 include/map_shortcodes.php:665
msgid "Theme"
msgstr ""

#: include/map_shortcodes.php:18
msgid "Logo Link?"
msgstr ""

#: include/map_shortcodes.php:30
msgid "Main Menu"
msgstr ""

#: include/map_shortcodes.php:39
msgid "Upload sticky logo image"
msgstr ""

#: include/map_shortcodes.php:58
msgid "Language, Currency Switcher"
msgstr ""

#: include/map_shortcodes.php:68
msgid "Countdown time"
msgstr ""

#: include/map_shortcodes.php:76 theme-config.php:640 theme-config.php:646
msgid "Social Icons"
msgstr ""

#: include/map_shortcodes.php:86
msgid "Mini Cart"
msgstr ""

#: include/map_shortcodes.php:96
msgid "Product Search"
msgstr ""

#: include/map_shortcodes.php:115 theme-config.php:873
msgid "Brand Logos"
msgstr ""

#: include/map_shortcodes.php:124 include/map_shortcodes.php:173
#: include/map_shortcodes.php:250
msgid "Number of columns"
msgstr ""

#: include/map_shortcodes.php:126 include/map_shortcodes.php:175
msgid "6"
msgstr ""

#: include/map_shortcodes.php:132 include/map_shortcodes.php:181
#: include/map_shortcodes.php:258
msgid "Number of rows"
msgstr ""

#: include/map_shortcodes.php:146
msgid "List Sub Categories"
msgstr ""

#: include/map_shortcodes.php:155
msgid "Category slug"
msgstr ""

#: include/map_shortcodes.php:164
msgid "Categories Carousel"
msgstr ""

#: include/map_shortcodes.php:195
msgid "Newsletter Form (MailPoet)"
msgstr ""

#: include/map_shortcodes.php:204
msgid "Form ID"
msgstr ""

#: include/map_shortcodes.php:207
msgid "Enter form ID here"
msgstr ""

#: include/map_shortcodes.php:214
msgid "Latest posts"
msgstr ""

#: include/map_shortcodes.php:223
msgid "Number of posts"
msgstr ""

#: include/map_shortcodes.php:225
msgid "5"
msgstr ""

#: include/map_shortcodes.php:231
msgid "Image scale"
msgstr ""

#: include/map_shortcodes.php:242
msgid "Excerpt length"
msgstr ""

#: include/map_shortcodes.php:244
msgid "20"
msgstr ""

#: include/map_shortcodes.php:252
msgid "4"
msgstr ""

#: include/map_shortcodes.php:272 theme-config.php:1243
msgid "Testimonials"
msgstr ""

#: include/map_shortcodes.php:281
msgid "Number of testimonial"
msgstr ""

#: include/map_shortcodes.php:283
msgid "10"
msgstr ""

#: include/map_shortcodes.php:289
msgid "Display Author"
msgstr ""

#: include/map_shortcodes.php:300
msgid "Display Avatar"
msgstr ""

#: include/map_shortcodes.php:311
msgid "Avatar image size"
msgstr ""

#: include/map_shortcodes.php:314
msgid "Avatar image size in pixels. Default is 50"
msgstr ""

#: include/map_shortcodes.php:320
msgid "Display URL"
msgstr ""

#: include/map_shortcodes.php:331
msgid "Category"
msgstr ""

#: include/map_shortcodes.php:333
msgid "0"
msgstr ""

#: include/map_shortcodes.php:334
msgid "ID/slug of the category. Default is 0"
msgstr ""

#: include/map_shortcodes.php:342
msgid "Rotating tweets"
msgstr ""

#: include/map_shortcodes.php:351
msgid "Twitter user name"
msgstr ""

#: include/map_shortcodes.php:360
msgid "Twitter feed"
msgstr ""

#: include/map_shortcodes.php:369
msgid "Your Twitter Name(Without the \"@\" symbol)"
msgstr ""

#: include/map_shortcodes.php:377
msgid "Number Of Tweets"
msgstr ""

#: include/map_shortcodes.php:394
msgid "Google map"
msgstr ""

#: include/map_shortcodes.php:403
msgid "Map Height"
msgstr ""

#: include/map_shortcodes.php:405
msgid "400"
msgstr ""

#: include/map_shortcodes.php:406
msgid "Map height in pixels. Default is 400"
msgstr ""

#: include/map_shortcodes.php:412
msgid "Map Zoom"
msgstr ""

#: include/map_shortcodes.php:414
msgid "17"
msgstr ""

#: include/map_shortcodes.php:415
msgid "Map zoom level, min 0, max 21. Default is 17"
msgstr ""

#: include/map_shortcodes.php:421 include/map_shortcodes.php:469
#: include/map_shortcodes.php:517 include/map_shortcodes.php:565
#: include/map_shortcodes.php:613
msgid "Latitude"
msgstr ""

#: include/map_shortcodes.php:430 include/map_shortcodes.php:478
#: include/map_shortcodes.php:526 include/map_shortcodes.php:574
#: include/map_shortcodes.php:622
msgid "Longtitude"
msgstr ""

#: include/map_shortcodes.php:439 include/map_shortcodes.php:487
#: include/map_shortcodes.php:535 include/map_shortcodes.php:583
#: include/map_shortcodes.php:631
msgid "Address"
msgstr ""

#: include/map_shortcodes.php:442 include/map_shortcodes.php:490
#: include/map_shortcodes.php:538 include/map_shortcodes.php:586
#: include/map_shortcodes.php:634
msgid "If you donot enter coordinate, enter address here"
msgstr ""

#: include/map_shortcodes.php:449 include/map_shortcodes.php:497
#: include/map_shortcodes.php:545 include/map_shortcodes.php:593
#: include/map_shortcodes.php:641
msgid "Marker image"
msgstr ""

#: include/map_shortcodes.php:452
msgid "Upload marker image, image size: 40x46 px"
msgstr ""

#: include/map_shortcodes.php:459 include/map_shortcodes.php:507
#: include/map_shortcodes.php:555 include/map_shortcodes.php:603
#: include/map_shortcodes.php:651 theme-config.php:923
msgid "Description"
msgstr ""

#: include/map_shortcodes.php:462
msgid "Allowed HTML tags: a, i, em, br, strong, h1, h2, h3"
msgstr ""

#: include/map_shortcodes.php:500 include/map_shortcodes.php:548
#: include/map_shortcodes.php:596 include/map_shortcodes.php:644
msgid "Upload marker image"
msgstr ""

#: include/map_shortcodes.php:510
msgid "Allowed HTML tags: a, i, em, br, strong, p, h2, h2, h3"
msgstr ""

#: include/map_shortcodes.php:558
msgid "Allowed HTML tags: a, i, em, br, strong, p, h3, h3, h3"
msgstr ""

#: include/map_shortcodes.php:606
msgid "Allowed HTML tags: a, i, em, br, strong, p, h4, h4, h4"
msgstr ""

#: include/map_shortcodes.php:654
msgid "Allowed HTML tags: a, i, em, br, strong, p, h5, h5, h5"
msgstr ""

#: include/map_shortcodes.php:662
msgid "Counter"
msgstr ""

#: include/map_shortcodes.php:671
msgid "Image icon"
msgstr ""

#: include/map_shortcodes.php:674
msgid "Upload icon image"
msgstr ""

#: include/map_shortcodes.php:680
msgid "Number"
msgstr ""

#: include/map_shortcodes.php:688
msgid "Text"
msgstr ""

#: include/nevarawidgets.php:8
msgid "Nevara Widgets"
msgstr ""

#: include/nevarawidgets.php:11
msgid "Display recent posts, comments, popular posts"
msgstr ""

#: include/nevarawidgets.php:128
msgid "says"
msgstr ""

#: include/nevarawidgets.php:169
msgid "Title:"
msgstr ""

#: include/nevarawidgets.php:173
msgid "Amount:"
msgstr ""

#: include/nevarawidgets.php:177
msgid "Image size (example: 50,50):"
msgstr ""

#: include/nevarawidgets.php:181
msgid "Widget Type:"
msgstr ""

#: include/nevarawidgets.php:183
msgid "Recent Posts"
msgstr ""

#: include/nevarawidgets.php:184
msgid "Popular Posts"
msgstr ""

#: include/nevarawidgets.php:185
msgid "Recent Comments"
msgstr ""

#: include/shortcodes.php:182
msgid "All Categories"
msgstr ""

#: include/shortcodes.php:446
#, php-format
msgid "%s"
msgstr ""

#: include/shortcodes.php:446
msgid "By: "
msgstr ""

#: include/wooajax.php:121 woocommerce/cart/mini-cart.php:36
#, php-format
msgid "%d"
msgstr ""

#: include/wooajax.php:140
msgid "Product is added to cart"
msgstr ""

#: include/wooajax.php:168
msgid "View Cart"
msgstr ""

#: index.php:98
msgid "No posts to display"
msgstr ""

#: index.php:102
#, php-format
msgid "Ready to publish your first post? <a href=\"%s\">Get started here</a>."
msgstr ""

#: search.php:69
#, php-format
msgid "Search Results for: %s"
msgstr ""

#: search.php:89
msgid ""
"Sorry, but nothing matched your search criteria. Please try again with some "
"different keywords."
msgstr ""

#: single.php:80
msgid "&larr;"
msgstr ""

#: single.php:81
msgid "&rarr;"
msgstr ""

#: tag.php:73
#, php-format
msgid "Tag Archives: %s"
msgstr ""

#: theme-config.php:81
msgid "Section via hook"
msgstr ""

#: theme-config.php:82
msgid ""
"<p class=\"description\">This is a section created by adding a filter to the "
"sections array. Can be used by child themes to add/remove sections from the "
"options.</p>"
msgstr ""

#: theme-config.php:144
#, php-format
msgid "Customize &#8220;%s&#8221;"
msgstr ""

#: theme-config.php:151 theme-config.php:154
msgid "Current theme preview"
msgstr ""

#: theme-config.php:161
#, php-format
msgid "By %s"
msgstr ""

#: theme-config.php:162
#, php-format
msgid "Version %s"
msgstr ""

#: theme-config.php:163
msgid "Tags"
msgstr ""

#: theme-config.php:168
#, php-format
msgid "This <a href=\"%1$s\">child theme</a> requires its parent theme, %2$s."
msgstr ""

#: theme-config.php:168
msgid "http://codex.wordpress.org/Child_Themes"
msgstr ""

#: theme-config.php:182
msgid "General"
msgstr ""

#: theme-config.php:183
msgid "General theme options"
msgstr ""

#: theme-config.php:192
msgid "Body background"
msgstr ""

#: theme-config.php:193
msgid "Upload image or select color. Only work with box layout"
msgstr ""

#: theme-config.php:200
msgid "Page content background"
msgstr ""

#: theme-config.php:201
msgid "Select background for page content (default: #ffffff)."
msgstr ""

#: theme-config.php:207
msgid "Border Option"
msgstr ""

#: theme-config.php:208
msgid "Only color validation can be done on this field type"
msgstr ""

#: theme-config.php:214
msgid "Back To Top"
msgstr ""

#: theme-config.php:215
msgid "Show back to top button on all pages"
msgstr ""

#: theme-config.php:224
msgid "Background top pages"
msgstr ""

#: theme-config.php:225
msgid "Select background for title breadcrumb (default: #f5f5f5)."
msgstr ""

#: theme-config.php:233
msgid "Colors"
msgstr ""

#: theme-config.php:234
msgid "Color options"
msgstr ""

#: theme-config.php:240
msgid "Primary Color"
msgstr ""

#: theme-config.php:241
msgid "Pick a color for primary color (default: #cc0000)."
msgstr ""

#: theme-config.php:251
msgid "Sale Label BG Color"
msgstr ""

#: theme-config.php:252
msgid "Pick a color for bg sale label (default: #cc0000)."
msgstr ""

#: theme-config.php:262
msgid "Sale Label Text Color"
msgstr ""

#: theme-config.php:263
msgid "Pick a color for sale label text (default: #ffffff)."
msgstr ""

#: theme-config.php:273
msgid "Rating Star Color"
msgstr ""

#: theme-config.php:274
msgid "Pick a color for star of rating (default: #f9bb00)."
msgstr ""

#: theme-config.php:283
msgid "Link Color"
msgstr ""

#: theme-config.php:284
msgid "Pick a color for link (default: #5be7c4)."
msgstr ""

#: theme-config.php:295
msgid "Text selected background"
msgstr ""

#: theme-config.php:296
msgid "Select background for selected text (default: #91b2c3)."
msgstr ""

#: theme-config.php:304
msgid "Text selected color"
msgstr ""

#: theme-config.php:305
msgid "Select color for selected text (default: #ffffff)."
msgstr ""

#: theme-config.php:316
msgid "Date Time Sale"
msgstr ""

#: theme-config.php:317
msgid "Date Time Sale options"
msgstr ""

#: theme-config.php:349
msgid "Header"
msgstr ""

#: theme-config.php:350
msgid "Header options"
msgstr ""

#: theme-config.php:357
msgid "Header Layout"
msgstr ""

#: theme-config.php:359 theme-config.php:599
msgid "Go to Visual Composer => Templates to create/edit layout"
msgstr ""

#: theme-config.php:368
msgid "Header background"
msgstr ""

#: theme-config.php:369 theme-config.php:609 theme-config.php:1278
msgid "Upload image or select color."
msgstr ""

#: theme-config.php:376
msgid "Header text color"
msgstr ""

#: theme-config.php:377
msgid "Pick a color for header color (default: #808080)."
msgstr ""

#: theme-config.php:385
msgid "Header link color"
msgstr ""

#: theme-config.php:386
msgid "Pick a color for header link color (default: #808080)."
msgstr ""

#: theme-config.php:399
msgid "Sticky header"
msgstr ""

#: theme-config.php:405
msgid "Use sticky header"
msgstr ""

#: theme-config.php:411
msgid "Header sticky background"
msgstr ""

#: theme-config.php:441
msgid "Top Bar"
msgstr ""

#: theme-config.php:449
msgid "Top bar text color"
msgstr ""

#: theme-config.php:450
msgid "Pick a color for top bar text color (default: #777777)."
msgstr ""

#: theme-config.php:459
msgid "Top bar link color"
msgstr ""

#: theme-config.php:460
msgid "Pick a color for top bar link color (default: #777777)."
msgstr ""

#: theme-config.php:473
msgid "Menu"
msgstr ""

#: theme-config.php:479
msgid "Mobile menu label"
msgstr ""

#: theme-config.php:480
msgid "The label for mobile menu (example: Menu, Go to..."
msgstr ""

#: theme-config.php:487
msgid "Submenu background"
msgstr ""

#: theme-config.php:488
msgid "Pick a color for sub menu bg (default: #1a1a1a)."
msgstr ""

#: theme-config.php:497
msgid "Submenu color"
msgstr ""

#: theme-config.php:498
msgid "Pick a color for sub menu color (default: #aaaaaa)."
msgstr ""

#: theme-config.php:514
msgid "Categories menu background"
msgstr ""

#: theme-config.php:515
msgid "Pick a color for categories menu background (default: #ffffff)."
msgstr ""

#: theme-config.php:523
msgid "Categories menu label"
msgstr ""

#: theme-config.php:524
msgid "The label for categories menu"
msgstr ""

#: theme-config.php:530
msgid "Number of items"
msgstr ""

#: theme-config.php:531
msgid "Number of menu items level 1 to show, default value: 6"
msgstr ""

#: theme-config.php:541
msgid "More items label"
msgstr ""

#: theme-config.php:542
msgid "The label for more items button"
msgstr ""

#: theme-config.php:548
msgid "Less items label"
msgstr ""

#: theme-config.php:549
msgid "The label for less items button"
msgstr ""

#: theme-config.php:555
msgid "Home Categories Menu"
msgstr ""

#: theme-config.php:556
msgid "Always show categories menu on home page"
msgstr ""

#: theme-config.php:562
msgid "Inner Categories Menu"
msgstr ""

#: theme-config.php:563
msgid "Always show categories menu on inner pages"
msgstr ""

#: theme-config.php:589
msgid "Footer"
msgstr ""

#: theme-config.php:590
msgid "Footer options"
msgstr ""

#: theme-config.php:597
msgid "Footer Layout"
msgstr ""

#: theme-config.php:608
msgid "Footer background"
msgstr ""

#: theme-config.php:616
msgid "Footer text color"
msgstr ""

#: theme-config.php:617
msgid "Pick a color for top bar text color (default: #808080)."
msgstr ""

#: theme-config.php:626
msgid "Footer link color"
msgstr ""

#: theme-config.php:627
msgid "Pick a color for footer link color (default: #808080)."
msgstr ""

#: theme-config.php:647
msgid "Enter social links"
msgstr ""

#: theme-config.php:648
msgid "Drag/drop to re-arrange"
msgstr ""

#: theme-config.php:685
msgid "Fonts"
msgstr ""

#: theme-config.php:686
msgid "Fonts options"
msgstr ""

#: theme-config.php:693
msgid "Body font"
msgstr ""

#: theme-config.php:701
msgid "Main body font."
msgstr ""

#: theme-config.php:714
msgid "Heading font"
msgstr ""

#: theme-config.php:723
msgid "Heading font."
msgstr ""

#: theme-config.php:734
msgid "Heading font style 2"
msgstr ""

#: theme-config.php:743
msgid "Heading font Style2."
msgstr ""

#: theme-config.php:754
msgid "Menu font"
msgstr ""

#: theme-config.php:763
msgid "Menu font."
msgstr ""

#: theme-config.php:775
msgid "Vertical Menu font"
msgstr ""

#: theme-config.php:784
msgid "Vertical Menu font."
msgstr ""

#: theme-config.php:796
msgid "Price font"
msgstr ""

#: theme-config.php:805
msgid "Price font."
msgstr ""

#: theme-config.php:819
msgid "Layout"
msgstr ""

#: theme-config.php:820
msgid "Select page layout: Box or Full Width"
msgstr ""

#: theme-config.php:827
msgid "Page Layout"
msgstr ""

#: theme-config.php:837
msgid "Box layout width"
msgstr ""

#: theme-config.php:838
msgid "Box layout width in pixels, default value: 1170"
msgstr ""

#: theme-config.php:848
msgid "Preset"
msgstr ""

#: theme-config.php:849
msgid "Select a preset to quickly apply pre-defined colors and fonts"
msgstr ""

#: theme-config.php:864
msgid "Show Style Switcher"
msgstr ""

#: theme-config.php:865
msgid "The style switcher is only for preview on front-end"
msgstr ""

#: theme-config.php:874
msgid "Upload brand logos and links"
msgstr ""

#: theme-config.php:880 theme-config.php:970 theme-config.php:1250
msgid "Auto scroll"
msgstr ""

#: theme-config.php:886
msgid "Scroll amount"
msgstr ""

#: theme-config.php:887
msgid "Number of logos to scroll one time, default value: 2"
msgstr ""

#: theme-config.php:897 theme-config.php:1220
msgid "Pause in (seconds)"
msgstr ""

#: theme-config.php:898 theme-config.php:1221
msgid "Pause time, default value: 3000"
msgstr ""

#: theme-config.php:908 theme-config.php:976 theme-config.php:1231
#: theme-config.php:1257
msgid "Animate in (seconds)"
msgstr ""

#: theme-config.php:909 theme-config.php:977 theme-config.php:1232
#: theme-config.php:1258
msgid "Animate time, default value: 2000"
msgstr ""

#: theme-config.php:919
msgid "Logos"
msgstr ""

#: theme-config.php:920
msgid "Upload logo image and enter logo link."
msgstr ""

#: theme-config.php:922 theme-config.php:990
#: woocommerce/class-wc-widget-cart.php:30
msgid "Title"
msgstr ""

#: theme-config.php:924 theme-config.php:992
msgid "Link"
msgstr ""

#: theme-config.php:932
msgid "Show more products"
msgstr ""

#: theme-config.php:933
msgid "Show more, show less products"
msgstr ""

#: theme-config.php:939
msgid "Products first show "
msgstr ""

#: theme-config.php:940
msgid "Value Items products first show, default value: 15"
msgstr ""

#: theme-config.php:950
msgid "Moreless products"
msgstr ""

#: theme-config.php:951
msgid "Value Items when show more (less more) , default value: 5"
msgstr ""

#: theme-config.php:963
msgid "Categories carousel"
msgstr ""

#: theme-config.php:964
msgid "Upload category logos and links"
msgstr ""

#: theme-config.php:987
msgid "Images"
msgstr ""

#: theme-config.php:988
msgid "Upload Categories image and enter categories link."
msgstr ""

#: theme-config.php:991
msgid "Number products"
msgstr ""

#: theme-config.php:1000
msgid "Sidebar"
msgstr ""

#: theme-config.php:1001
msgid "Sidebar options"
msgstr ""

#: theme-config.php:1008
msgid "Shop Sidebar Position"
msgstr ""

#: theme-config.php:1009
msgid "Sidebar on shop page"
msgstr ""

#: theme-config.php:1018
msgid "Pages Sidebar Position"
msgstr ""

#: theme-config.php:1019
msgid "Sidebar on pages"
msgstr ""

#: theme-config.php:1028
msgid "Blog Sidebar Position"
msgstr ""

#: theme-config.php:1029
msgid "Sidebar on Blog pages"
msgstr ""

#: theme-config.php:1038
msgid "Custom Sidebars"
msgstr ""

#: theme-config.php:1039
msgid "Add more sidebars"
msgstr ""

#: theme-config.php:1040
msgid ""
"Enter sidebar name (Only allow digits and letters). click Add more to add "
"more sidebar. Edit your page to select a sidebar "
msgstr ""

#: theme-config.php:1047
msgid "Product"
msgstr ""

#: theme-config.php:1048
msgid "Use this section to select options for product"
msgstr ""

#: theme-config.php:1054
msgid "Shop Layout"
msgstr ""

#: theme-config.php:1064
msgid "Shop default view"
msgstr ""

#: theme-config.php:1074
msgid "Products per page"
msgstr ""

#: theme-config.php:1075
msgid "Amount of products per page on category page"
msgstr ""

#: theme-config.php:1086
msgid "Product columns"
msgstr ""

#: theme-config.php:1087
msgid "Amount of product columns on category page"
msgstr ""

#: theme-config.php:1088 theme-config.php:1100
msgid "Only works with: 1, 2, 3, 4, 6"
msgstr ""

#: theme-config.php:1098
msgid "Product columns on full width shop"
msgstr ""

#: theme-config.php:1099
msgid "Amount of product columns on full width category page"
msgstr ""

#: theme-config.php:1110
msgid "Show Rating"
msgstr ""

#: theme-config.php:1111
msgid "Show rating on all pages"
msgstr ""

#: theme-config.php:1119
msgid "Product page"
msgstr ""

#: theme-config.php:1125
msgid "ShareThis/AddThis head tag"
msgstr ""

#: theme-config.php:1126
msgid "Paste your ShareThis or AddThis head tag here"
msgstr ""

#: theme-config.php:1132
msgid "ShareThis/AddThis code"
msgstr ""

#: theme-config.php:1133
msgid "Paste your ShareThis or AddThis code here"
msgstr ""

#: theme-config.php:1140 theme-config.php:1159
msgid "Quick View"
msgstr ""

#: theme-config.php:1146
msgid "View details text"
msgstr ""

#: theme-config.php:1152
msgid "View all features text"
msgstr ""

#: theme-config.php:1153
msgid "This is the text on quick view box"
msgstr ""

#: theme-config.php:1160
msgid "Show quick view button on all pages"
msgstr ""

#: theme-config.php:1168
msgid "Use this section to select options for blog"
msgstr ""

#: theme-config.php:1175
msgid "Blog header text"
msgstr ""

#: theme-config.php:1181
msgid "Blog Layout"
msgstr ""

#: theme-config.php:1191
msgid "Read more text"
msgstr ""

#: theme-config.php:1197
msgid "Excerpt length on blog page"
msgstr ""

#: theme-config.php:1208
msgid "Latest posts carousel"
msgstr ""

#: theme-config.php:1214
msgid "Latest posts auto scroll"
msgstr ""

#: theme-config.php:1244
msgid "Use this section to select options for Testimonials"
msgstr ""

#: theme-config.php:1269
msgid "Error 404 Page"
msgstr ""

#: theme-config.php:1270
msgid "Error 404 page options"
msgstr ""

#: theme-config.php:1277
msgid "Error 404 background"
msgstr ""

#: theme-config.php:1303
msgid "Less Compiler"
msgstr ""

#: theme-config.php:1304
msgid ""
"Turn on this option to apply all theme options. Turn of when you have "
"finished changing theme options and your site is ready."
msgstr ""

#: theme-config.php:1310
msgid "Enable Less Compiler"
msgstr ""

#: theme-config.php:1317
msgid "<strong>Theme URL:</strong> "
msgstr ""

#: theme-config.php:1318
msgid "<strong>Author:</strong> "
msgstr ""

#: theme-config.php:1319
msgid "<strong>Version:</strong> "
msgstr ""

#: theme-config.php:1323
msgid "<strong>Tags:</strong> "
msgstr ""

#: theme-config.php:1328
msgid "Import / Export"
msgstr ""

#: theme-config.php:1329
msgid "Import and Export your Redux Framework settings from file, text or URL."
msgstr ""

#: theme-config.php:1344
msgid "Theme Information"
msgstr ""

#: theme-config.php:1360
msgid "Theme Information 1"
msgstr ""

#: theme-config.php:1361 theme-config.php:1367
msgid "<p>This is the tab content, HTML is allowed.</p>"
msgstr ""

#: theme-config.php:1366
msgid "Theme Information 2"
msgstr ""

#: theme-config.php:1371
msgid "<p>This is the sidebar content, HTML is allowed.</p>"
msgstr ""

#: theme-config.php:1391 theme-config.php:1392
msgid "Theme Options"
msgstr ""

#: woocommerce/archive-product.php:106
msgid "View on"
msgstr ""

#: woocommerce/archive-product.php:107
msgid "Grid"
msgstr ""

#: woocommerce/archive-product.php:108
msgid "List"
msgstr ""

#: woocommerce/cart/cart.php:81
msgid "Available on backorder"
msgstr ""

#: woocommerce/cart/cart.php:121 woocommerce/cart/mini-cart.php:77
msgid "Remove this item"
msgstr ""

#: woocommerce/cart/cart.php:139
msgid "Update cart"
msgstr ""

#: woocommerce/cart/cart.php:157
msgid "Enter your coupon code if you have one."
msgstr ""

#: woocommerce/cart/cart.php:158
msgid "Coupon code"
msgstr ""

#: woocommerce/cart/cart.php:158
msgid "Apply coupon"
msgstr ""

#: woocommerce/cart/mini-cart.php:32
msgid "My cart"
msgstr ""

#: woocommerce/cart/mini-cart.php:72
msgid "Qty"
msgstr ""

#: woocommerce/checkout/form-checkout.php:29
msgid "You must be logged in to checkout."
msgstr ""

#: woocommerce/class-wc-widget-cart.php:23
msgid "Display the user's Cart in the sidebar."
msgstr ""

#: woocommerce/class-wc-widget-cart.php:25
msgid "WooCommerce Cart"
msgstr ""

#: woocommerce/class-wc-widget-cart.php:29
#: woocommerce/class-wc-widget-cart.php:54
msgid "Cart"
msgstr ""

#: woocommerce/class-wc-widget-cart.php:35
msgid "Hide if cart is empty"
msgstr ""

#: woocommerce/content-product-archive.php:105
#: woocommerce/content-product.php:60
msgid "Sale"
msgstr ""

#: woocommerce/loop/orderby.php:26
msgid "Sort By"
msgstr ""

#: woocommerce/single-product/meta.php:31
msgid "SKU:"
msgstr ""

#: woocommerce/single-product/meta.php:31
msgid "N/A"
msgstr ""

#: woocommerce/single-product/meta.php:35
msgid "Category:"
msgstr ""

#: woocommerce/single-product/meta.php:37
msgid "Tag:"
msgstr ""

#: woocommerce/single-product/related.php:31
msgid "Related products"
msgstr ""

#: woocommerce/single-product/up-sells.php:31
msgid "Upsell products"
msgstr ""
