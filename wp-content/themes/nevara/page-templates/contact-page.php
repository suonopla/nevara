<?php
/**
 * Template Name: Contact Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Huge Shop consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Nevara_Theme
 * @since Huge Shop 1.0
 */

$nevara_opt = get_option( 'nevara_opt' );

get_header();
?>
<div class="main-container contact-page">
	<div class="title-breadcrumb">
		<div class="container">
			<div class="title-breadcrumb-inner">
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
				<?php Nevara_Class::nevara_breadcrumb(); ?>
			</div>
		</div>
	</div>
	<div class="page-content">
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
					<div class="entry-content">
						<?php the_content(); ?>
					</div><!-- .entry-content -->
				
				</article><!-- #post -->
			<?php endwhile; // end of the loop. ?>
	</div>
</div>
<?php
if(isset($nevara_opt['enable_map']) && $nevara_opt['enable_map']) :
	//Add google map API
	wp_enqueue_script( 'gmap-api-js', 'http://maps.google.com/maps/api/js?sensor=false' , array(), '3', false );
	// Add jquery.gmap.js file
	wp_enqueue_script( 'jquery.gmap-js', get_template_directory_uri() . '/js/jquery.gmap.js', array(), '2.1.5', false );

	$map_desc = str_replace(array("\r\n", "\r", "\n"), "<br />", $nevara_opt['map_desc']);
	$map_desc = addslashes($map_desc);
	$theme_uri = get_template_directory_uri();
	wp_enqueue_script( 'contact-page.js', get_template_directory_uri() . '/js/contact-page.js' , array(), '1.0.0', false );
	wp_localize_script('contact-page.js','data',array(
		'theme_uri'=>$theme_uri,
		'opt'=>$nevara_opt,
		'map_desc'=>$map_desc
	));
endif; ?>
<?php get_footer('contact'); ?>