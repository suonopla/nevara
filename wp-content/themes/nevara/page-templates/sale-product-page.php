<?php
/**
 * Template Name: Sale product page
 *
 * Description: About page template
 *
 * @package WordPress
 * @subpackage Nevara_Theme
 * @since Huge Shop 1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
get_header( 'shop' );

global $wp_query, $woocommerce_loop;

$nevara_opt = get_option( 'nevara_opt' );
$shoplayout = 'sidebar';
if(isset($nevara_opt['shop_layout']) && $nevara_opt['shop_layout']!=''){
	$shoplayout = $nevara_opt['shop_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$shoplayout = $_GET['layout'];
}
$shopsidebar = 'left';
if(isset($nevara_opt['sidebarshop_pos']) && $nevara_opt['sidebarshop_pos']!=''){
	$shopsidebar = $nevara_opt['sidebarshop_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$shopsidebar = $_GET['sidebar'];
}
switch($shoplayout) {
	case 'fullwidth':
		Nevara_Class::nevara_shop_class('shop-fullwidth');
		$shopcolclass = 12;
		$shopsidebar = 'none';
		$productcols = 4;
		break;
	default:
		Nevara_Class::nevara_shop_class('shop-sidebar');
		$shopcolclass = 9;
		$productcols = 3;
}

$nevara_viewmode = Nevara_Class::nevara_show_view_mode();
?>
<div class="main-container woocommerce">
	<div class="page-content"> 
		<div class="shop-desc <?php echo esc_attr($shoplayout);?>">
			<div class="title-breadcrumb">
				<div class="container">
					<div class="title-breadcrumb-inner">
						<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
							<header class="entry-header">
								<h1 class="entry-title"><?php woocommerce_page_title(); ?></h1>
							</header>
						<?php endif; ?>
						<?php
							/**
							 * woocommerce_before_main_content hook
							 *
							 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
							 * @hooked woocommerce_breadcrumb - 20
							 */
							do_action( 'woocommerce_before_main_content' );
						?>
					</div>
				</div>
			</div>
		</div>
		<?php 

		global $wp_query;

		$wp_query->set( 'post_type', 'product');
		$wp_query->set( 'page_id', '' );
        $wp_query->set( 'page', '' );
        $wp_query->set( 'pagename', '' );

		$meta_query = array( array(
            'key'     => '_sale_price',
            'value'   => 0,
            'compare' => '>',
            'type'    => 'numeric'
        ) );

		//$wp_query->set( 'meta_query', $meta_query );

		$args = array(
		    'post_type'      => 'product',
		    'meta_query'     => array(
		        'relation' => 'OR',
		        array( // Simple products type
		            'key'           => '_sale_price',
		            'value'         => 0,
		            'compare'       => '>',
		            'type'          => 'numeric'
		        ),
		        array( // Variable products type
		            'key'           => '_min_variation_sale_price',
		            'value'         => 0,
		            'compare'       => '>',
		            'type'          => 'numeric'
		        )
		    )
		);

		// if ( isset( $_GET['max_price'] ) || isset( $_GET['min_price'] ) ) { // WPCS: input var ok, CSRF ok.
		// 	$meta_query                 = wc_get_min_max_price_meta_query( $_GET ); // WPCS: input var ok, CSRF ok.
		// 	$meta_query['price_filter'] = true;

		// 	$args['price_filter'] = $meta_query;
		// }

		// $q->set( 'posts_per_page', $q->get( 'posts_per_page' ) ? $q->get( 'posts_per_page' ) : apply_filters( 'loop_shop_per_page', wc_get_default_products_per_row() * wc_get_default_product_rows_per_page() ) );
		
		$wp_query->is_archive           = true;
        $wp_query->is_post_type_archive = true;
        $wp_query->is_singular          = false;
       	$wp_query->is_page              = false;

		//$wc_query = WC()->query;

    	//$wc_query->product_query( $wp_query );

    	//$wc_query->remove_product_query();

       	$wp_query->query = $wp_query->query_vars;
    	$wp_query->get_posts();

		?>
		<div class="shop_content">
			<div class="container"> 
				<div class="row">
					<?php if( $shopsidebar == 'left' ) :?>
						<?php if ( is_active_sidebar( 'sidebar-sale-product' ) ) : ?>
						<div id="secondary" class="col-xs-12 col-md-3 sidebar-shop">
							<?php dynamic_sidebar( 'sidebar-sale-product' ); ?>
						</div>
						<?php endif; ?>
						
					<?php endif; ?>
					<div id="archive-product" class="col-xs-12 <?php echo 'col-md-'.$shopcolclass; ?>">   
						<div class="archive-border">								
							<?php
								/**
								* remove message from 'woocommerce_before_shop_loop' and show here
								*/
								do_action( 'woocommerce_show_message' );
							?>							
							<?php if ( have_posts() ) : ?>								
								<?php woocommerce_product_loop_start(); ?>
								<div class="shop-products products <?php echo esc_attr($nevara_viewmode);?> <?php echo esc_attr($shoplayout);?>">									
									<?php $woocommerce_loop['columns'] = $productcols; ?>									
									<?php woocommerce_product_subcategories();
									//reset loop
									$woocommerce_loop['loop'] = 0; ?>									
									<?php if ( woocommerce_products_will_display() ) { ?>
										<div class="toolbar">
											<div class="toolbar-inner">
												<div class="view-mode">
													<label><?php esc_html_e('View on', 'nevara');?></label>
													<a href="#" class="grid <?php if($nevara_viewmode=='grid-view'){ echo ' active';} ?>" title="<?php echo esc_html__( 'Grid', 'nevara' ); ?>"></a>
													<a href="#" class="list <?php if($nevara_viewmode=='list-view'){ echo ' active';} ?>" title="<?php echo esc_html__( 'List', 'nevara' ); ?>"></a>
												</div>
												<?php
													/**
													 * woocommerce_before_shop_loop hook
													 *
													 * @hooked woocommerce_result_count - 20
													 * @hooked woocommerce_catalog_ordering - 30
													 */
													do_action( 'woocommerce_before_shop_loop' );
												?>
												<div class="clearfix"></div>
											</div>
										</div>
									<?php } ?> 
									<?php while ( have_posts() ) : the_post(); ?>
										<?php wc_get_template_part( 'content', 'product-archive' ); ?>
									<?php endwhile; // end of the loop. ?>  
								</div>
								<?php //woocommerce_product_loop_end(); ?>								
								<?php if ( woocommerce_products_will_display() ) { ?>
								<?php
									/**
									 * woocommerce_before_shop_loop hook
									 *
									 * @hooked woocommerce_result_count - 20
									 * @hooked woocommerce_catalog_ordering - 30
									 */
									do_action( 'woocommerce_after_shop_loop' );
									//do_action( 'woocommerce_before_shop_loop' );
								?>
								<div class="clearfix"></div>								
								<?php } ?>								
							<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
								<?php wc_get_template( 'loop/no-products-found.php' ); ?>
							<?php endif; ?>

						<?php
							/**
							 * woocommerce_after_main_content hook
							 *
							 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
							 */
							do_action( 'woocommerce_after_main_content' );
						?>

						<?php
							/**
							 * woocommerce_sidebar hook
							 *
							 * @hooked woocommerce_get_sidebar - 10
							 */
							//do_action( 'woocommerce_sidebar' );
						?>
						</div>
					</div>
					<?php if($shopsidebar == 'right') :?>
						<?php //get_sidebar('sidebar-sale-product'); ?>
						<?php //get_sidebar('content-sale-product'); ?>
					<?php endif; ?>
				</div>
			</div> 
		</div>
	</div>
</div>
<?php get_footer( 'shop' ); ?>