<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Nevara_Theme
 * @since Huge Shop 1.0
 */

$nevara_opt = get_option( 'nevara_opt' );

get_header();

$nevara_bloglayout = 'nosidebar';
if(isset($nevara_opt['blog_layout']) && $nevara_opt['blog_layout']!=''){
	$nevara_bloglayout = $nevara_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$nevara_bloglayout = $_GET['layout'];
}
$nevara_blogsidebar = 'right';
if(isset($nevara_opt['sidebarblog_pos']) && $nevara_opt['sidebarblog_pos']!=''){
	$nevara_blogsidebar = $nevara_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$nevara_blogsidebar = $_GET['sidebar'];
}
switch($nevara_bloglayout) {
	case 'sidebar':
		$nevara_blogclass = 'blog-sidebar';
		$nevara_blogcolclass = 9;
		Nevara_Class::nevara_post_thumbnail_size('nevara-category-thumb');
		break;
	case 'largeimage':
		$nevara_blogclass = 'blog-large';
		$nevara_blogcolclass = 9;
		$nevara_postthumb = '';
		break;
	default:
		$nevara_blogclass = 'blog-nosidebar';
		$nevara_blogcolclass = 12;
		$nevara_blogsidebar = 'none';
		Nevara_Class::nevara_post_thumbnail_size('nevara-post-thumb');
}
?>
<div class="main-container">
	<div class="title-breadcrumb">
		<div class="container">
			<div class="title-breadcrumb-inner">
				<header class="entry-header">
					<h1 class="entry-title"><?php if(isset($nevara_opt)) { echo esc_attr($nevara_opt['blog_header_text']); } else { esc_html_e('Blog', 'nevara');}  ?></h1>
				</header>
				<?php Nevara_Class::nevara_breadcrumb(); ?>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<?php if($nevara_blogsidebar=='left') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
			
			<div class="col-xs-12 <?php echo 'col-md-'.$nevara_blogcolclass; ?>">
			
				<div class="page-content blog-page <?php echo esc_attr($nevara_blogclass); if($nevara_blogsidebar=='left') {echo ' left-sidebar'; } if($nevara_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<?php if ( have_posts() ) : ?>
						
						<header class="archive-header">
							<h1 class="archive-title"><?php printf( wp_kses(esc_html__( 'Search Results for: %s', 'nevara' ), array('span'=>array())), '<span>' . get_search_query() . '</span>' ); ?></h1>
						</header><!-- .archive-header -->

						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content', get_post_format() ); ?>
						<?php endwhile; ?>

						<div class="pagination">
							<?php Nevara_Class::nevara_pagination(); ?>
						</div>

					<?php else : ?>

						<article id="post-0" class="post no-results not-found">
							<header class="entry-header">
								<h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'nevara' ); ?></h1>
							</header>

							<div class="entry-content">
								<p><?php esc_html_e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'nevara' ); ?></p>
								<?php get_search_form(); ?>
							</div><!-- .entry-content -->
						</article><!-- #post-0 -->

					<?php endif; ?>
				</div>
			</div>
			<?php if( $nevara_blogsidebar=='right') : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
		
	</div>
</div>
<?php get_footer(); ?>