<?php
/**
 * The sidebar containing the main widget area
 *
 * If no active widgets are in the sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Nevara_Theme
 * @since Huge Shop 1.0
 */

$nevara_opt = get_option( 'nevara_opt' );
 
$nevara_blogsidebar = 'right';
if(isset($nevara_opt['sidebarblog_pos']) && $nevara_opt['sidebarblog_pos']!=''){
	$nevara_blogsidebar = $nevara_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$nevara_blogsidebar = $_GET['sidebar'];
}
?>
<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
	<div id="secondary" class="col-xs-12 col-md-3 <?php echo ($nevara_blogsidebar=='left')?'pull-left': 'pull-right' ?>">
		<div class="sidebar-border <?php echo esc_attr($nevara_blogsidebar);?>">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div>
	</div><!-- #secondary -->
<?php endif; ?>