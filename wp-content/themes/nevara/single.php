<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Nevara_Theme
 * @since Huge Shop 1.0
 */

$nevara_opt = get_option( 'nevara_opt' );

get_header();

$nevara_bloglayout = 'nosidebar';
if(isset($nevara_opt['blog_layout']) && $nevara_opt['blog_layout']!=''){
	$nevara_bloglayout = $nevara_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$nevara_bloglayout = $_GET['layout'];
}
$nevara_blogsidebar = 'right';
if(isset($nevara_opt['sidebarblog_pos']) && $nevara_opt['sidebarblog_pos']!=''){
	$nevara_blogsidebar = $nevara_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$nevara_blogsidebar = $_GET['sidebar'];
}
switch($nevara_bloglayout) {
	case 'sidebar':
		$nevara_blogclass = 'blog-sidebar';
		$nevara_blogcolclass = 9;
		break;
	default:
		$nevara_blogclass = 'blog-nosidebar'; //for both fullwidth and no sidebar
		$nevara_blogcolclass = 12;
		$nevara_blogsidebar = 'none';
}
?>
<div class="main-container page-wrapper">
	<div class="title-breadcrumb">
		<div class="container">
			<div class="title-breadcrumb-inner">
				<header class="entry-header">
					<h1 class="entry-title"><?php if(isset($nevara_opt)) { echo esc_attr($nevara_opt['blog_header_text']); } else { esc_html_e('Blog', 'nevara');}  ?></h1>
				</header>
				<?php Nevara_Class::nevara_breadcrumb(); ?>
			</div>
		</div>
		
	</div>
	<div class="container">
		<div class="row">

			<?php
			$customsidebar = get_post_meta( $post->ID, '_nevara_custom_sidebar', true );
			$customsidebar_pos = get_post_meta( $post->ID, '_nevara_custom_sidebar_pos', true );

			if($customsidebar != ''){
				if($customsidebar_pos == 'left' && is_active_sidebar( $customsidebar ) ) {
					echo '<div id="secondary" class="col-xs-12 col-md-3">';
						dynamic_sidebar( $customsidebar );
					echo '</div>';
				} 
			} else {
				if($nevara_blogsidebar=='left') {
					get_sidebar();
				}
			} ?>
			
			<div class="col-xs-12 <?php echo 'col-md-'.$nevara_blogcolclass; ?>">
				<div class="page-content blog-page single <?php echo esc_attr($nevara_blogclass); if($nevara_blogsidebar=='left') {echo ' left-sidebar'; } if($nevara_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', get_post_format() ); ?>

						<?php comments_template( '', true ); ?>
						
						<!--<nav class="nav-single">
							<h3 class="assistive-text"><?php esc_html_e( 'Post navigation', 'nevara' ); ?></h3>
							<span class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'nevara' ) . '</span> %title' ); ?></span>
							<span class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'nevara' ) . '</span>' ); ?></span>
						</nav>-->
						
					<?php endwhile; // end of the loop. ?>
				</div>
			</div>
			<?php
			if($customsidebar != ''){
				if($customsidebar_pos == 'right' && is_active_sidebar( $customsidebar ) ) {
					echo '<div id="secondary" class="col-xs-12 col-md-3">';
						dynamic_sidebar( $customsidebar );
					echo '</div>';
				} 
			} else {
				if($nevara_blogsidebar=='right') {
					get_sidebar();
				}
			} ?>
		</div>
	</div> 
</div>

<?php get_footer(); ?>